import Model, { attr } from '@ember-data/model';

export default class MsgModel extends Model {

    @attr('string') name;
    @attr('string') body;


}
