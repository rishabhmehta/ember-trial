import EmberRouter from '@ember/routing/router';
import config from 'text/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('msg', function() {
    this.route('new', {path: '/:new'});
  });
  // this.route('msg');
});
