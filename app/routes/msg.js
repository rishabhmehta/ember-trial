import Route from '@ember/routing/route';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class MsgRoute extends Route {

    @service store;
  model() {
    return this.store.findAll('msg');
  }

  //   @service store;
  // async () {
  //   this.store.findRecord('msg')
  // }
}
