import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class MsgNewController extends Controller {

    @action ms(){
        var newmsg = this.createRecord('message', {
                name: this.get('name'),
                body: this.get('body')
            });
            newmsg.save();
            this.setProperties({
                name: '',
                body: ''
            })
    }
}
