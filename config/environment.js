'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'text',
    environment,
    rootURL: '/',
    locationType: 'auto',
    firebase: {
      apiKey: "AIzaSyApuzapHIJT29TO92eBbAprTSjwsP4dYTw",
    authDomain: "ember-trial-e9eb9.firebaseapp.com",
    databaseURL: "https://ember-trial-e9eb9.firebaseio.com",
    projectId: "ember-trial-e9eb9",
    storageBucket: "ember-trial-e9eb9.appspot.com",
    messagingSenderId: "321637806459",
    appId: "1:321637806459:web:fcfbe53697bb5ba1c38dde",
    measurementId: "G-Y417KLLYBQ",
    },
    EmberENV: {
     
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
