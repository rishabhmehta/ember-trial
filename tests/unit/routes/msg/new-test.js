import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | msg/new', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:msg/new');
    assert.ok(route);
  });
});
